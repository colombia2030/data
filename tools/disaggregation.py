import csv
from tempfile import NamedTemporaryFile
import shutil
from pathlib import Path


filename = '../indicators.csv'
tempfile =  NamedTemporaryFile(mode='w+', delete=False,encoding='utf-8')
#tempfile = '../indicators2.csv'
with open(filename, newline='',encoding='utf-8') as csvfile, tempfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar='"')
	writer = csv.writer(tempfile, delimiter=',', quotechar='"',lineterminator='\n',quoting=csv.QUOTE_NONNUMERIC)

	i = 0
	for row in reader:
		my_file = Path('../indicators/'+row[2]+'.csv')		
		if my_file.is_file():
			with open('../indicators/'+row[2]+'.csv', newline='',encoding='utf-8') as csvindfile:
				spamreader2 = csv.reader(csvindfile, delimiter=';', quotechar='|')
				headers = next(spamreader2)[0:]
				row[10] = (', '.join(headers)[10:])				
		writer.writerow(row)
shutil.move(tempfile.name, filename)
