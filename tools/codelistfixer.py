from glob import glob
import json
import os
import csv

class ObjFile(object):
	def toJSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)
class ObjCode(object):
	def toJSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)

codelistfiles = glob("../metadata/dimensions/*.csv")
files = []
for codelistfile in codelistfiles:	
	filename = os.path.splitext(os.path.basename(codelistfile))[0]

	with open(codelistfile, newline='',encoding='utf-8') as csvfile:
		spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
		for row in spamreader:			
			current = None
			flag = 0
			for file in files:				
				if(file.name==(row[2])):
					flag = 1
					current = file
			if(flag == 0):
				current = ObjFile()
				current.name = row[2];
				print(row[2])
				current.codes = []
				files.append(current)
			code = ObjCode()			
			code.name = row[1]
			code.value = row[0]
			current.codes.append(code)

for f in files:
		with open("../metadata/dimensions/CL/"+f.name+'.csv', 'w', newline='',encoding='utf-8') as csvfile2:    		
			writer = csv.writer(csvfile2 , delimiter = ',',quoting=csv.QUOTE_NONNUMERIC)
			for c in f.codes:
				writer.writerow([c.value, c.name])
